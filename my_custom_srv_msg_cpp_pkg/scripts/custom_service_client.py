#! /usr/bin/env python
import rospkg
import rospy
from my_custom_srv_msg_cpp_pkg.srv import MyCustomServiceMessage, MyCustomServiceMessageRequest, MyCustomServiceMessageResponse # you import the service message python classes 
 
 
rospy.init_node('custom_srv_client_node')
rospy.wait_for_service('/my_service') 
custom_service_client = rospy.ServiceProxy('/my_service', MyCustomServiceMessage) # Create the connection to the service
custom_srv_request_object = MyCustomServiceMessageRequest()
result = custom_service_client(custom_srv_request_object)
print result